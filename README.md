This repo contains some of the code Andoni wrote while working on this project that I (Arthur) was able to find on the server.  

In brief, he wrote code to download all the batches from the library of congress website, extract those batches, and parse the xml to get the
textblocks.  

However, the size of this data set made it really hard to work with. He only parsed the xml for about 1/7th of the batches he had downloaded. In addition, he was never able to create a DTM, finsish ldatuning, or run a topic model.

This code is here simply so that we can reference it if any questions come up and **should not be the basis for future code** now that we have changed our approach.


